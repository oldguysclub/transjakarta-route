from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class RuteHalte(models.Model):
    id = models.OneToOneField(User, on_delete=models.CASCADE)
    nama_halte = models.CharField(max_length = 40)
    jurusan = models.CharField(max_length = 40)
    koridor = models.CharField(max_length = 120)
    geomtex = models.CharField(max_length = 120)
class Halte(models.Model):
    HalteName = models.OneToOneField(User, on_delete=models.CASCADE)
    KoridorNo = models.CharField(max_length = 40)
    Long = models.CharField(max_length = 40)
    Lat = models.CharField(max_length = 120)
    HalteType = models.CharField(max_length = 120)
