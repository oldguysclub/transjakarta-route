from django.shortcuts import render
import json

import folium

# Create your views here.
def index(request):
        m = folium.Map(location=[-6.194393, 106.821391], zoom_start=12)
        map = m.get_root().render()
        
        context = {'map': map}

        return render(request, 'TJRoute_app/index.html', context)