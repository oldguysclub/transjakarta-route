from django.apps import AppConfig


class TjrouteAppConfig(AppConfig):
    name = 'TJRoute_app'
